const express = require('express')
const cors = require('cors')
const proxy = require('express-http-proxy')
const chalk = require('chalk')

const kyc = require('./apis/kyc')

const { format } = require('date-fns')

const app = express()
const cluster = require('cluster')
const fs = require('fs')

const corsOptions = {
  credentials: true,
  allowedHeaders: 'content-type',
  origin: 'https://www.tiktok.com',
}

app.use(function (req, res, next) {
  let coloredMethod = req.method;
  switch (req.method) {
    case 'GET':
      coloredMethod = chalk.green(padded(req.method));
      break;
    case 'POST':
    case 'PUT':
    case 'PATCH':
      coloredMethod = chalk.blue(padded(req.method));
      break;
    case 'DELETE':
      coloredMethod = chalk.red(padded(req.method));
      break;
    case 'OPTIONS': coloredMethod = chalk.yellow(padded(req.method));
      break;
  }
  res.set('Content-Type', 'application/json')
  console.log(`${format(new Date(), 'mm:ss:SSS')} ${coloredMethod}  ${unescape(req.url)} `);
  next()
})
app.use(cors(corsOptions))
app.use('/kyc', kyc)

function padded(s) {
  if (s.length !== 7) return padded(' ' + s);
  return s;
}

const port = 3333

if (cluster.isMaster) {
  // console.log('master ', process.pid);
  let worker = null
  worker = cluster.fork();

  function restart() {
    if (worker) {
      worker.kill()
    }
    worker = cluster.fork()
  }

  fs.watch('./apis', {recursive: true}, restart)
  fs.watchFile('./server.js', restart)

  cluster.on('exit', (worker, code, signal) => {
    console.log('worker %d died (%s)',
      worker.process.pid, signal || code);

    if (code) {
      console.log('error');
      worker = null
    }
  });
} else {
  // console.log('child ', process.pid);
  app.listen(port, () => console.log(
    `Operator mock API server is listening on port ${port}!`))
}
