# Setup

Setup Charles according to https://bytedance.feishu.cn/wiki/wikcn2RAj4vwVyLHJixqvSC2DXe

Import `./noah.xml` remote mapping into Charles.

Charles will direct all the https traffic to the mock API server running at http://localhost:3333 according to the config. 


## Handle the request
Mock API server can then choose from these choices:

1. Return some response
2. Forward request to localhost (devServer)
3. Forward request to remote server (passthrough)



## Express HTTP Proxy
To learn how to proxy request, you can refer to more examples at https://www.npmjs.com/package/express-http-proxy#options 
