const proxy = require('express-http-proxy')
const express = require('express')
const { Router } = express
const router = Router()


router.use('/account', proxy('https://login-dmall-staging.bytedance.net', {
  proxyReqPathResolver: req => '/account' + req.url,
}))

router.use('/oauth2', proxy('https://login-dmall-staging.bytedance.net', {
  proxyReqPathResolver: req => '/oauth2' + req.url,
  userResHeaderDecorator(headers, userReq, userRes, proxyReq, proxyRes) {
    headers['Access-Control-Allow-Origin'] = '*'
    // recieves an Object of headers, returns an Object of headers.
    return headers;
  }
}))

module.exports = router

