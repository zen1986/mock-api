const express = require('express')
const init = require('../../jsons/init.json')
const { Router } = express
const proxy = require('express-http-proxy')
const router = Router()
const byted = require('./index')
const bodyParser = require('body-parser')

// portal API
const base = 'http://10.27.36.31:9261'
router.use('/api/v1', proxy(base, {
// router.use('/api/v1', proxy('https://day-one-staging.bytedance.net', {
  // https: true,
  proxyReqPathResolver: req => '/api/v1' + req.url,
}))

module.exports = router

