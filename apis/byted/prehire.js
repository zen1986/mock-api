const express = require('express')
const init = require('../../jsons/init.json')
const { Router } = express
const proxy = require('express-http-proxy')
const router = Router()
const byted = require('./index')
const bodyParser = require('body-parser')
const saas = require('./saas-prehire')




// router.use('/saas', saas)

// router.get('/api/v1/info/basic', (req, res) => {
//   res.json({
//     name: 'derek zeng', 
//     email: 'dere@zeng.com',
//     mobileNumber: 12323123132,
//     workCountry: 'sg',
//     onboardingDetails: {
//       timestamp: 123213213, location: 'sg'
//     },
//     locale: 'en-US',
//     jobTitle: 'whatever',
//   })
// })


router.get('/api/v1/form', (req, res, next) => {
  const query = req.query
  // if (query.task_name==='task_choose_laptop') {
  //   const form = byted.read('staging/sg/prehire/laptop.yaml')
  //   res.json({form_json: form})
  // }
  // else
  if (query.task_name==='task_personal_info') {
    // let form = byted.read('staging/sg/prehire/personal_info-base.yaml')
    // form = JSON.parse(form)
    // form.fields_meta.forEach(f => f.disabled = false)

    res.json({form_json: byted.read('personalinfo.json')}) // JSON.stringify(form)})
  }
  // else
  // if (query.task_name === 'task_sign_and_upload_ea') {
  //   const form = byted.read('staging/sg/prehire/employment_docs.yaml')
  //   res.json({form_json: form})
  // }
  // else
  // if (query.task_name === 'task_confirm_work_email') {
  //   const form = byted.read('staging/sg/prehire/email_prefix.yaml')
  //   res.json({form_json: form})
  // }
  else {
    next()
  }
  // res.send(403)
})

// router.get('/api/v1/tasks/status', (req, res) => {
//   res.json(byted.read('status.json', true))
// })

// router.get('/api/v1/init', (req, res) => {
//   res.json(byted.read('init.json', true))
// })

// router.get('/api/v1/tasks/next', (req, res) => {
//   res.json({"task_name":"task_sign_and_upload_ea"})
// })

// router.get('/api/v1/tasks/status', (req, res) => {
//   res.json(byted.read('status.json', true))
// })

// router.use('/api/v1/form', bodyParser.json())

// router.post('/api/v1/form', (req, res) => {
//   res.send(200)
// })
//   if (req.body.draft) {
//     res.send(200)
//   } else {
//     res.status(400).json(
//       {
//         "form_errors": {
//           "PersonalInfo__WorkExperience__1__StartDate": [
//             "Start date must be before end date"
//           ]
//         },
//         "form_errors_i18n": {
//           "PersonalInfo__WorkExperience__1__StartDate": [
//             {
//               "en-US": "Start date must be before end date",
//               "zh-CN": "\u5f00\u59cb\u65e5\u671f\u5fc5\u987b\u65e9\u4e8e\u7ed3\u675f\u65e5\u671f"
//             }
//           ]
//         }
//       })
//   }
// })
//   // res.status(405)
//   res.json(
//     {
//       "form_errors": {
//         "BankingInfo__HasLocalBankAccount": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__Addresses": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__CitizenshipStatus": [
//           "must be set"
//         ],
//         "PersonalInfo__DateOfBirth": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__Education": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__EmailAddress": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__EmergencyContacts": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__Gender": [
//           "must be set"
//         ],
//         "PersonalInfo__MaritalInfo__Status": [
//           "must be set"
//         ],
//         "PersonalInfo__Nationality": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__PhoneNumberLS": [
//           "cannot be empty"
//         ],
//         "PersonalInfo__Race": [
//           "must be set"
//         ],
//         "PersonalInfo__WorkExperience": [
//           "cannot be empty"
//         ]
//       }
//     }
//   )
// })


// portal API
// router.use('/api/v1', proxy('https://day-one-staging.bytedance.net', {
//   https: true,
//   proxyReqPathResolver: req => '/api/v1' + req.url,
//   // userResDecorator: (proxyRes, proxyResData, userReq, userRes) => {
//   //   if (userReq.url.match('task_personal_info')) {
//   //     const data = JSON.parse(proxyResData.toString('utf8'))
//   //     const formData = JSON.parse(data.form_json)
//   //     // formData.data.work_force_type = '2'
//   //     formData.fields_meta.forEach(m => {
//   //       m.required = false
//   //       // if (/EmploymentDocs__EAFinalSigned__FileIDs/.test(m.field_name))  {
//   //         m.disabled = false
//   //         m.writable = true
//   //         // delete m.value
//   //         // delete m.raw_value
//   //       // }
//   //     })

//   //     formData.data.is_my_form = true
//   //     formData.data.is_sg_form = false
//   //     formData.data.work_visa_required = false
//   //     formData.data.form_country_code = 'my'
//   //     formData.data.work_force_type = '1'
//   //     formData.page.steps.push(
//   //       { 
//   //         title: {
//   //           "en-US": "Country Statutory Information",
//   //           "zh-CN": "国家法定信息"
//   //         },
//   //         sections: [ 16, 17, 18, 19, 20, 21, 22 ]
//   //       }
//   //     )
//   //     data.form_json = JSON.stringify(formData)
//   //     return data
//   //   }
//   //   return proxyResData
//   // }
// }))

// drive API
// router.use('/drive/api/v1', proxy('https://day-one-staging.bytedance.net', {
//   https: true,
//   proxyReqPathResolver: req => '/drive/api/v1' + req.url
// }))

// router.use(proxy('http://localhost:8966'))
router.use(proxy('https://day-one-staging.bytedance.net', {
  https: true,
}))

module.exports = router
