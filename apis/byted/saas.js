const proxy = require('express-http-proxy')
const express = require('express')
const { Router } = express
const router = Router()


router.use('/people/api/saas', proxy('https://dmall-staging.bytedance.net', {proxyReqPathResolver: req => '/people/api/saas' + req.url}))
router.use('/people/api', proxy('https://people-staging.bytedance.net', {proxyReqPathResolver: req => '/people/api' + req.url})) 
router.use(proxy('http://localhost:8966'))

module.exports = router
