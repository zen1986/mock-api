const express = require('express')
const { omit, flatMap }  = require('lodash')
const YAML = require('js-yaml');
const path = require('path')
const fs = require('fs')
const init = require('../../jsons/init.json')
const preference = require('../../jsons/preference.json')
const saas = require('./saas')
const { Router } = express
const proxy = require('express-http-proxy')
const { parseYaml, read } = require('./helper')
const router = Router()


router.use('/app-onboarding-web', proxy('http://localhost:8977', { proxyReqPathResolver: req => '/onboarding/' + req.url}))
router.use(/\/app-onboarding-settings-web/, proxy('http://localhost:9000',  { proxyReqPathResolver: req => '/onboarding-settings/index.html'}))
// router.use('/app-onboarding-settings-web', proxy('http://localhost:9000', { proxyReqPathResolver: req => req.url}))
router.use(proxy('https://people-staging.bytedance.net', {
  https: true,
  proxyReqPathResolver: req => '/people/' + req.url
}))


module.exports = router
