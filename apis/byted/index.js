const express = require('express')
const { omit, flatMap }  = require('lodash')
const YAML = require('js-yaml');
const path = require('path')
const fs = require('fs')
const init = require('../../jsons/init.json')
const preference = require('../../jsons/preference.json')
const saas = require('./saas')
const { Router } = express
const proxy = require('express-http-proxy')
const { parseYaml, read } = require('./helper')
const microfe = require('./micro')
const router = Router()

// const built = false
// if (built) {
   // router.use('/__webpack_hmr', proxy('http://localhost:9000', {proxyReqPathResolver: req => '/__webpack_hmr'}))

//   // for routes, serve the index.html
//   //https://lf1-cdn-tos.bytegoofy.com/goofy/corehr-notif/
//   router.use(/\/people\/app-onboarding-settings-web/, proxy('http://localhost:8080', {proxyReqPathResolver: req => '/template/index.html'}))
//   // for assets
//   router.use('/onboarding-static', proxy('http://localhost:8080', {proxyReqPathResolver: req => '/resource' + req.url}))
//   // for API
//   router.use('/cnfgs', proxy('https://people-staging.bytedance.net/', {
//     https: true,
//     proxyReqPathResolver: req => '/cnfgs' + req.url,
//   }))
//   router.use(proxy('https://people-staging.bytedance.net', {
//     https: true,
//   }))
// } else {
  // router.get('/cnfgs/forms', (req, res) => {
  //   res.json({
  //     data: [
  //       {
  //         id: '123',
  //         name: {
  //           'zh-CN': 'Personal information',
  //           'en-US': 'Personal information',
  //         },
  //         modifiedBy: {
  //           "avatar": "https://p3-ee-corehr-staging.byteimg.com/tos-cn-i-ipt45x0tb2/552385c957ea4dcbb2f5dc581c36cef1~tplv-ipt45x0tb2-image.image",
  //           "id": "6891251722631890445",
  //           "employeeNumber": "9960875111",
  //           "email": "I5Ec-Rq4Uw@testpersonal.test",
  //           "name": {
  //             "zh-CN": "赵二"
  //           },
  //           "lang": "en-US",
  //           "department": {
  //             "id": "6893001523433833998",
  //             "name": {
  //               "en-US": "Corporate Services",
  //               "zh-CN": "企业服务部门"
  //             }
  //           }
  //         },
  //         modifiedTime: Date.now()
  //       }
  //     ]
  //   })
  // })

// router.get('/cnfgs/onboarding/tasks/api/v1/notifications/:id', (req, res) => {
//   res.json(read('saas/notifs.json', true)['tasks'].find(t => t.id === req.params.id))
// })

// router.get('/cnfgs/onboarding/tasks/api/v1/notifications', (req, res) => {
//   res.json(read('saas/notifs.json', true))
// })

  // router.use('/people/app-onboarding-settings-web', proxy('http://localhost:9000', {
  //   proxyReqPathResolver: req => '/people/app-onboarding-settings-web/' + req.url,
  // }))

// }



// router.get('/api/saas/onboarding/api/v1/hr/pre-hire/:id', (req, res) => {
//   res.send(200)
// })
// router.get('/api/saas/onboarding/api/v1/hr/special-action/metadata', (req, res) => {
//   res.json(read('saas/meta_cols.json'))
// })
// router.get('/api/saas/onboarding/api/v1/hr/tracker/init', (req, res) => {
//   res.json(read('saas/init.json'))
// })

// router.get('/people/api/saas/onboarding/api/v1/hr/tracker/pre-hires', (req ,res) => {
//   res.json(read('saas/prehire.json'))
// })

// router.use('/people/app-onboarding-web', proxy('http://localhost:8977', {
//   proxyReqPathResolver: req => '/people/app-onboarding-web' + req.url,
// }))

// router.use('/people/api/saas/onboarding', proxy('https://people-staging.bytedance.net', {
//   proxyReqPathResolver: req => '/people/api/saas/onboarding' + req.url,
//   proxyReqOptDecorator: function(proxyReqOpts, srcReq) {
//     // you can update headers
//     proxyReqOpts.headers['Cookie'] = 'passport_web_did=6982512170995073026; locale=en-US; landing_url=https://passport.feishu-pre.cn/suite/passport/page/login/?app_id=35&query_scope=all&redirect_uri=https%3A%2F%2Fpeople.feishu-pre.cn%2Fpeople%2Fteam; __tea__ug__uid=6982512162367686159; swp_csrf_token=73f28ac6-976b-4069-9de7-23e46b021122; t_beda37=2c57f34924cfb6d8fed55c7088df33a26ccc9e1823d677db346a2cf27da84a8c; session=XN0YXJ0-08950bf6-211a-42cc-a246-6a8ccdfb255g-WVuZA; MONITOR_WEB_ID=f1d45f49bf7b0e24d6e2d6d959ffdff8; admin-csrf-token=OExxZ6Ok+moOluQQQIY3ce2CmWCizKw3lWai2kjBJ3to2Ib2kkTIfxOXsPLksOUThOmPyML/cGRO+RCAM3XV8XiAfUxNs0mHaqkq0TkkwmKl65Fk/LY65kAfBox1TS+lRAtPVg=='
//     return proxyReqOpts;
//   }
// }))
// router.use(proxy('https://people-staging.bytedance.net', {
//   https: true,
// }))
// router.use('/prehires/api/v1/prehire-forms', (req, res) => {
//   const personalinfo = read('forms/personal-info.json')
//   const form = {
//     "tenant_id": "task_personal_info",
//     "candidate_id": "6950191181061948964",
//     "task_id": "task_personal_info",
//     "workflow_node_name": "task_review_personal_info",
//     "form_json": personalinfo,
//     "is_submittable": true
//   }
//   res.json(form)
// })

// router.use('/prehires/api/v1/candidates/:id/laptopInfo', (req, res) => {
//   res.json(read('laptop.json', true))
// })
router.use('/onboarding', proxy('http://localhost:8977', { proxyReqPathResolver: req => '/onboarding/' + req.url}))

// router.use('/people/hr-settings/onboarding', proxy('http://localhost:9000'))
// router.use(proxy('https://people-staging.bytedance.net', {
//   https: true,
// }))

router.use(proxy('https://people-staging.bytedance.net', {
  https: true,
}))

module.exports = router
router.read = read
