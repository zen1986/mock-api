const { flatMap } = require('lodash')
const YAML = require('js-yaml')
const path = require('path')
const fs = require('fs')

/**
 * @param string p
 */
function read(p, parse = false) {
  const yaml = p.indexOf('yaml') >= 0
  const yamlSchemaFolder = '/Users/derekzeng/go/src/noah-form/resources/'
  let str
  if (yaml) {
    str = fs.readFileSync(path.resolve(yamlSchemaFolder + p), {
      encoding: 'utf-8',
    })
  } else {
    str = fs.readFileSync(path.resolve(__dirname, `../../jsons/${p}`), {
      encoding: 'utf-8',
    })
  }
  if (parse) {
    if (yaml) {
      return parseYaml(str)
    } else {
      return JSON.parse(str)
    }
  } else {
    if (yaml) {
      return JSON.stringify(parseYaml(str))
    } else {
      return str
    }
  }
}

function parseYaml(str) {
  const parsed = YAML.load(str)
  const {
    schema: { fields: fields_ },
  } = parsed
  function d(field) {
    if (field.type === 'field') {
      return [
        {
          field_name: field.name,
          ...field.field_meta,
        },
      ]
    }
    let ret = []
    if (field.type === 'list') {
      const { name, items } = field

      for (let i = 0; i < 1; i++) {
        const itemField = {
          name: name + '__' + i + '',
          ...items,
        }
        ret = ret.concat(d(itemField))
      }
    } else {
      const { name, fields } = field
      for (const f of fields || []) {
        const fs = d(f)
        fs.forEach((ff) => (ff.field_name = name + '__' + ff.field_name))
        ret = ret.concat(fs)
      }
    }
    return ret
  }

  return {
    ui_meta: parsed.ui_meta,
    page: parsed.page,
    version: parsed.version,
    data: parsed.data,
    fields_meta: flatMap(fields_, d),
  }
}

exports.parseYaml = parseYaml
exports.read = read
