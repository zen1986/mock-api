const proxy = require('express-http-proxy')
const express = require('express')
const { Router } = express
const { read } = require('./helper')
const router = Router()

// let i = 0

// router.get('/people/api/saas/onboarding/api/v1/hr/pre-hire/:id', (req, res) => {
//   res.sendStatus(500)
// })
// router.get('/people/api/saas/onboarding/api/v1/hr/special-action/metadata', (req, res) => {
//   res.json(read('saas/actions.json', true))
// })
// router.get('/people/onboarding/api/v1/pre-hire/user-info', (req, res) => {
//   // res.json(read('saas/user-info.json', true))
//   res.json({
//     lang: 'zh-CN',
//     location: 'https://google.com'
//   })
// })
// router.post(
//   '/people/api/saas/onboarding/api/v1/hr/special-action',
//   (req, res) => {
//     res.json(read('saas/actions.json', true))
//   }
// )
// router.get('/people/api/saas/onboarding/api/v1/hr/tracker/filters', (req, res) => {
//   res.json(read('saas/filters.json', true))
// })
// router.get('/people/api/saas/onboarding/api/v1/hr/tracker/metadata', (req, res) => {
//   res.json(read('saas/meta_cols.json', true))
// })
// router.get('/people/api/saas/onboarding/api/v1/hr/preferences', (req, res) => {
//   res.json({})
// })
// router.get('/people/api/saas/onboarding/api/v1/hr/tracker/pre-hires', (req, res) => {
//   // res.sendStatus(500).json({message: 'wtf'})
//   res.json(read('saas/prehire.json', true))
// })
// router.use('/people/api/saas/onboarding/api/v1/hr/pre-hire/:iid/form/:id', (req, res) => {
//   res.sendStatus(200)
// })
// router.use('/people/onboarding/api/v1/pre-hire/form/:form_instance_id', (req, res) => {
//   res.sendStatus(400)
// })
// router.post('/people/onboarding/api/v1/pre-hire/form/:formid', (req, res) => res.status(500).send('hello'))
// router.post('/people/api/saas/onboarding/api/v1/hr/pre-hire/:id/form/:formid', (req, res) => res.status(500).send('hello'))
// router.get('/people/onboarding/api/v1/pre-hire/user-info', (req, res) => {
//   res.sendStatus(403)
//   // return res.json(read('saas/user.json'))
// }) 
// router.get('/people/app-onboarding-settings-web', (req, res) => {
//   res.sendStatus(500)
// })
//
// router.use('/people/api/saas/onboarding/task_center/api/v1/metadata', (req, res) => {
//   res.json(read('settings/meta.json', true))
// })
// router.use('/people/api/saas/onboarding/task_center/api/v1/notifications/:id', (req, res) => {
//   res.json(read('settings/notif.json', true))
// })
// router.use('/people/api/saas/onboarding/task_center/api/v1/notifications', (req, res) => {
//   res.json(read('settings/notifications.json', true))
// })
// router.use('/people/api/saas/onboarding/task_center/api/v1/hr/temp/form_editor/form_meta', (req, res) => {
//   res.json(read('settings/forms.json', true))
// })
// router.use('/people/api/saas/onboarding/task_center/api/v1/metadata/trees/department', (req, res) => {
//   res.json(read('saas/departments.json', true))
// })

// router.use('/people/api/saas/onboarding/task_center/api/v1/metadata/trees/location', (req, res) => {
//   res.json(read('saas/locations.json', true))
// })

// router.use('/people/api/saas/onboarding/task_center/api/v1/banners/:id', (req, res) => {
//   res.json(read('saas/banner.json', true))
// })

// router.use('/people/api/saas/onboarding/task_center/api/v1/alerts', (req, res) => {
//   res.json(read('settings/alerts.json', true))
// })
// router.use('/people/onboarding/api/v1/pre-hire/form-configurator/bpm-token', (req, res) => {
//   setTimeout(() => proxy(`https://${req.hostname}`)(req, res), 5000)
// })

// router.use('/people/onboarding/api/v1/pre-hire/user-info', (req, res) => {
//   res.json(read('saas/prehire-user-info.json', true))
// })
// router.use('/people/onboarding/api/v1/pre-hire/tasks', (req, res) => {
//   res.json(read('saas/tasks.json', true))
// })

router.use('/people/app-onboarding-web', proxy('http://localhost:8977', {proxyReqPathResolver: req => '/people/app-onboarding-web' + req.url}))
router.use('/people/api/saas/onboarding/api/v1/hr/user-info', (req, res) => {
  res.json(read('saas/hr-user-info.json', true))
})

// router.use('/people/onboarding/bpm/form/preFormInstanceCheck', (req, res) => {
//   setTimeout(() => res.json({}), 100000)
// })

// PREHIRE DETOUR/MASTER
// router.use('/people/welcome', proxy('http://localhost:8966', {proxyReqPathResolver: req => '/people/welcome' + req.url}))

// PREHIRE MASTER
// router.use('/people/welcome/assets', proxy('http://localhost:8966', {proxyReqPathResolver: req => '/people/welcome/assets' + req.url}))
// router.use('/people/welcome', proxy('http://localhost:8966', {proxyReqPathResolver: req => '/people/welcome/assets/index.html'}))


// router.use('/people/api/onboarding/api/v1/hr/form-configurator/bpm-token', (req, res) => {
//   res.json({token: 'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SmhjSEJmYVdRaU9pSTRPRGc0SWl3aVpYaHdJam94TmpReE9EYzJOak16ZlEuT01xWGZQeFAxUkc1b3Itdy1OZ0JuU0RoRDNQZFdYXzdhbEY5cDF4a0hoQQ=='})
// })
router.use('/people/api/onboarding/config/form/list', (req, res) => {
  res.json({
    code: 0,
    message: 'ok',
    data: [
      {
        flowId: 123,
        formId: '7048907966707404558',
        formCloneId: '7048907966719856414',
        formName: {
          'zh-CN': '流程',
          'en-US': 'flow',
        },
        operatorList: {
          canDel: true,
          canEdit: true,
          canCopy: true,
        },
      },
    ],
  })
})
router.use('/people/api/onboarding/config/form/create', (req, res) => {
  res.json({
    code: 0,
    message: 'ok',
    data: {
      flowId: 123,
      formId: '7048907966707404558',
      formCloneId: '7048907966719856414',
      formName: {
        'zh-CN': '流程',
        'en-US': 'flow',
      },
      operatorList: {
        canDel: true,
        canEdit: true,
        canCopy: true,
      },
    },
  })
})


router.use('/__webpack_hmr', proxy('http://localhost:9000', {proxyReqPathResolver: req => '/__webpack_hmr'}))
router.use('/people/app-onboarding-settings-web', (req, res, next) => {
  if (req.url.includes('.')) {
    return proxy('http://localhost:9000', {proxyReqPathResolver: req => '/people/app-onboarding-settings-web/' + req.url})(req, res, next)
  }
  return proxy('http://localhost:9000', {proxyReqPathResolver: req => '/people/app-onboarding-settings-web/index.html'})(req, res, next)
})


// router.use('/people/hr-settings/onboarding', 
//            proxy('http://localhost:9000', {proxyReqPathResolver: req => '/people/app-onboarding-settings-web/index.html'}))


router.use((req, res, next) => {
  return proxy(`https://${req.hostname}`, {limit: '10mb'})(req, res, next)
})

module.exports = router

