const express = require('express')
const { each, last } = require('lodash')
const init = require('../../jsons/init.json')
const { Router } = express
const proxy = require('express-http-proxy')
const router = Router()
const byted = require('./index')
const bodyParser = require('body-parser')

// router.get('/api/v1/form', (req, res, next) => {
//   const query = req.query
//   if (query.task_name==='task_choose_laptop') {
//     const form = byted.read('staging/sg/prehire/laptop.yaml')
//     res.json({form_json: form})
//   }
//   else
//     if (query.task_name==='task_personal_info') {
//     const form = byted.read('staging/sg/prehire/personal_info.yaml')
//     res.json({form_json: form})
//   }
//   else if (query.task_name === 'task_sign_and_upload_ea') {
//     const form = byted.read('staging/sg/prehire/employment_docs.yaml')
//     res.json({form_json: form})
//   }
//   else if (query.task_name === 'task_confirm_work_email') {
//     const form = byted.read('staging/sg/prehire/email_prefix.yaml')
//     res.json({form_json: form})
//   }
//   else {
//     next()
//     return
//   }
// })

// router.get('/api/v1/tasks/status', (req, res) => {
//   res.json(byted.read('status.json', true))
// })

// router.get('/api/v1/init', (req, res) => {
//   res.json(init)
// })
// router.use('/api/v1/form', bodyParser.json())
// let count = 0
// router.post('/api/v1/form', (req, res) => {
//   count ++
//   if (count % 4 !== 0) {
//     res.send(200)
//   } else {
//     res.status(400).json(
//       {
//         "form_errors_i18n": {
//           "PersonalInfo__EmergencyContacts__0__Relationship": [
//             {
//               "en-US": "Start date must be before end date"
//             }
//           ],
//           "PersonalInfo__EmergencyContacts__0__PhoneNumberLS": [
//             {
//               "en-US": "Start date must be before end date"
//             }
//           ]
//         }
//       })
//   }
// })

router.use('/api/v1', proxy('https://day-one-tiktok-staging.bytedance.net', {
  https: true,
  proxyReqPathResolver: req => '/api/v1' + req.url,
  // userResDecorator: (proxyRes, proxyResData, userReq, userRes) => {
  //   if (userReq.url.match('task_personal_info')) {
  //     const data = JSON.parse(proxyResData.toString('utf8'))
  //     const formData = JSON.parse(data.form_json)
  //     formData.fields_meta.forEach(m => {
  //       // if (/BusinessUnitID/.test(m.field_name))  {
  //         m.disabled = false
  //         m.writable = true
  //         m.required = false
  //       // }
  //     })
      
  //     // formData.data.is_my_form = true
  //     // formData.data.is_sg_form = false
  //     // formData.data.form_country_code = 'my'
  //     // formData.data.work_country_id = 'CN_11'
  //     // each(formData.fields_meta, f => {
  //     //   f.required = false
  //     // })
  //     // formData.page.steps = formData.page.steps.slice(0, 2)

  //     // formData.data.work_force_type = '2'
  //     // const field = formData.fields_meta.find(m => m.field_name === 'PersonalInfo__HighestEducationCertificateFileIDs')

  //     // field.multiple = false

  //     data.form_json = JSON.stringify(formData)
  //     return data
  //   }
  //   return proxyResData
  // }
}))

router.use('/drive/api/v1', proxy('https://day-one-tiktok-staging.bytedance.net', {
  https: true,
  proxyReqPathResolver: req => '/drive/api/v1' + req.url
}))

router.use(proxy('http://localhost:8966'))

router.use(proxy('https://day-one-tiktok-staging.bytedance.net', {
  https: true,
}))

module.exports = router

