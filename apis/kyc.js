const proxy = require('express-http-proxy')
const byted = require('./byted/index')
const express = require('express')
const { Router } = express
const router = Router()

let submitted = 0

router.get(
  '/webcast/wallet_api_tiktok/income_plus/get_user_region_info/',
  (req, res) => {
    res.json({
      status_code: 0,
      data: {
        country_or_region_code: 'GB',
        country_or_region_name: 'Great Britain',
        ttregion: 'gb',
      },
    })
  }
)

router.get('/accepted', (req, res) => {
  res.json(byted.read('acceptedIds.json', true))
})

router.post('/webcast-t/api/compliance/kyc/v1/info/submit', (req, res) => {
  submitted = 1
  // return proxy('https://webcast-t.tiktok.com', {
  //   proxyReqPathResolver: req => req.url.replace('webcast-t', 'webcast').replace('compliance', 'money'),
  // })(req, res)
  // res.setHeader('Access-Control-Allow-Headers', '*')
  res.json({
    base_resp: {
      status_code: 0
    }
  })
})

// kyc status
router.post('/webcast-t/api/compliance/kyc/v1/info/detail', (req, res) => {
  if (submitted) {
    res.json({
      kyc_status: {
        created: true,
        user_id: 1234,
        cdd_status: 1, 
        // screen_status?: SCREEN_STATUS
        // added_in_kyc_list_time?: number
        // rejection_reason: "wrong this, wrong that"
      },
      base_resp: {
        status_message: '',
        status_code: 0,
      },
    })
  } else {
    res.json({
      kyc_status: {
        created: true,
        user_id: 1234,
        cdd_status: 7, 
        // screen_status?: SCREEN_STATUS
        // added_in_kyc_list_time?: number
        rejection_reason: "wrong this, wrong that"
      },
      base_resp: {
        status_message: '',
        status_code: 0,
      },
    })
  }
  // res.json({
  //   kyc_status: {
  //     user_id: 'abc',
  //     created: false,
  //   },
  //   base_resp: {
  //     status_message: '',
  //     status_code: 0,
  //   },
  // })
})

router.get('/webcast-t/api/compliance/kyc/v1/submission', (req, res) => {
  res.json({
    full_name: 'derek',
    birthday: 'zeng',
    nationality: 'us',
    id_type: 'PASSPORT',

    base_resp: {
      status_message: '',
      status_code: 0,
    },
  })
})

router.post('/webcast-t/api/compliance/kyc/v1/upload_file', proxy('https://webcast-t.tiktok.com', {
  parseReqBody: false, 
  proxyReqPathResolver: req => req.url.replace('webcast-t', 'webcast').replace('compliance', 'money'),
  https: true,
}))

module.exports = router
