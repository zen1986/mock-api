const dayjs = require('../../utils')
const { fromPairs } = require('lodash')
const mock = require('mockjs')


const rates = {}
const currencies = ['SGD', 'RUB', 'USD']
const today = dayjs.utc()

for (let i=0;i<10;i++) {
  rates[today.add(i, 'days').format('YYYY-MM-DD')] = {
    ...fromPairs(currencies.map(c => [c, mock.Random.float]))
  }
}

export default rates

