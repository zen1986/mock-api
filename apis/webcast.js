// const proxy = require('express-http-proxy')
const express = require('express')
const { Router } = express
const router = Router()


router.get('/get_user_region_info', (req, res) => {
  res.json({
    status_message: 'hello world',
    status_code: 0,
    data: {
      country_or_region_name: 'US', 
      country_or_region_code: 'us',
    }
  })
})

router.post('/tax/get_tax', (req, res) => {
  res.json({
    status_message: 'submitted',
    status_code: 200
  })
})


router.post('/tax/create_tax', (req, res) => {

})

module.exports = router


